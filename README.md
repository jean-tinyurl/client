# Get Start with tinyUrl tools - Frondend

## Framework
- React ( with create-react-app )
- Typescript

## Methods to start the project

### Preprocess:
- Install npm and docker
- Install git and set the gitlab account

### Start and Stop Project
1. `git clone` this project to your local
2. Run web
    - Choice 1: Run the Project Locally
        - `npm install`: Install the dependencies and generate the /node_modules
        - `npm run build`: Build the web and generate the /build
        - `npm run start`: Run web on `http://localhost:3000`
    - Choice 2: run the project in docker container
        - `docker build -t web .`: Build the docker image based on dockerfile
        - `docker run -it -p 3000:3000 --name web web`: Run the container based on the docker image
        - The web also run on `http://localhost:3000`

3. Start web: `ctrl + c`

### Notes:
1. The repo only includes frontend part, please check backend source codes in: `https://gitlab.com/jean-tinyurl/api`
2. The web also deploy to heroku: `https://tinyurl-jean.herokuapp.com/`

### Todo: Refine UIUX