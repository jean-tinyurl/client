import React, { useState } from "react";
import config from "./config.json";

function App() {
    const [original, setOriginal] = useState("");
    const onOriginalChange = e => setOriginal(e.target.value);
    const [tinyUrl, setTiny] = useState("");

    const handleSubmit = e => {
        e.preventDefault();

        const data = {original};
        const requestOptions = {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(data)
        };
        console.log("inside handleGetJson");
        fetch(`${config.domain}/createTiny`, requestOptions)
            .then( async (response) => {
                
                if(response.ok) {
                    let data = await response.text();
                    console.log("response", data);
                    setTiny(data);
                    // alert(`Copy the TinyUrl\r\r${data}`);
                    return data;
                }
                // throw new Error("Network response was not ok.");
            })
            .catch(function(error) {
                console.log("There has been a problem with your fetch operation: ", error.message);
            });
          
    };

    return (
        <div className="App">
            <label className="topic">TinyUrl tools</label>
            <p/>
            <form>
                <label>Pull in your url</label>
                <br/>
                <input placeholder="url" value={original}
                    onChange={onOriginalChange} required />
                <button className="box" type="submit" onClick={handleSubmit}>
                Create Post
                </button>
                <br/>
                <label>Below is the TinyUrl!</label>
                <br/>
                <input type="text" disabled={true} placeholder="tinyurl" value={tinyUrl}
                    onChange={handleSubmit} required />
            </form>
        </div>
    );
}

export default App;
