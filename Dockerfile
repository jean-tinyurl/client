FROM node:16.8.0

RUN apt-get update && apt-get install -y git vim bash wget npm
RUN git clone https://gitlab.com/jean-tinyurl/client.git

WORKDIR /client
RUN npm install
EXPOSE 3000

CMD ["npm", "run", "start"]
